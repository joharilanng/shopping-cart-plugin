/**
* 	Converts an easy-to-read JS cart object ('cart') into SiteCatalysts' weirdo product and event strings.
*	@ author Johari Lanng / Digital Balance <johari@digitalbalance.com.au>
*/
function shoppingCart() {
	
	// Fix lack of IE console	
	if (!window.console) console = {log: function() {}}; 
	if (!window.console.group) console.group = {log: function() {}}; 
	if (!window.console.groupEnd) console.groupEnd = {log: function() {}}; 

	// CONFIG START ---------------------------------------------------------------
	debugging = true;				// set to true for debugging output to console.

	// don't touch these!
	tmpProducts = new Array;
    tmpEvents = new Array;
    cartTotal = 0;
	
	// configure these variables with appropriate evar, prop and event names from report suite.
	evars = {
			product: "eVar44",
			productID: "eVar17",
			discount: "eVar32",
			shipping: "eVar33",
			voucher: "eVar34",
			currentCart: "eVar18",
			cartToDate: "eVar19"
	}

	events = {
			product: "event29",
			discount: "event28",
			shipping: "event29",
			voucher: "event30"		
	}

	props = {
			productID: "prop20"
	}
    // CONFIG END.
    
/* LOOP THRU CART ITEMS ----------------------------------------------------
* loop through all the cart items, start adding these items to either the 
* temporary product or event arrays.
*/
	cart = s.contextData["cart"];
    for(x=0; x < cart.length; x++) {
      switch(cart[x].type) {
            case 'product':     var tmpProduct = ['',cart[x].id,cart[x].units,cart[x].amount];
              					
            					// if present, add product-level discount as an evar
              					if(!isNaN(cart[x].discountAmount)){
            						tmpProduct.push(events.discount + "=" + cart[x].discountAmount);
            					}            					
            					if(typeof(cart[x].name) !== "undefined"){
            						tmpProduct.push(evars.discount + "=" + cart[x].name);
            					}
            					
                                tmpProducts.push(tmpProduct.join(';'));
                                cartTotal += cart[x].amount;			// ADD product price to cartTotal.
                                break;
                                
            case 'shipping':    tmpProducts.push(";;;;" + events.shipping + "=" + cart[x].amount + ";" + evars.shipping + "=" + cart[x].description);
                                tmpEvents.push(events.shipping);                                                                     
                                cartTotal += cart[x].amount;	// ADD shipping to cartTotal.
                                break; 
                                
            case 'voucher':     tmpProducts.push(";;;;" + events.voucher + "=" + cart[x].amount +";"+ evars.voucher + "=" + cart[x].description);                                                                                  
                                tmpEvents.push(events.voucher);
                                cartTotal -= cart[x].amount;		// SUBTRACT voucher from cartTotal.
                                break;
                                
            case 'discount':    //discount = [cart[x].discountType.replace(';','-'),cart[x].discountAmount];
                                tmpProducts.push(";;;;" + events.discount + "=" + cart[x].amount + ";" + evars.discount + "=" + cart[x].description);
                                tmpEvents.push(events.discount);                                                                                    
                                // cartTotal -= cart[x].discountAmount;	// SUBTRACT discount from cartTotal.
                                break;
        }   
    }
    
/* BUILD FINAL S.PRODUCTS -----------------------------------------------------
// collapse tmpProdArray into a string and assign to s.products.
*/
    s.products = tmpProducts.join();
    
    // BUILD S.EVENTS -------------------------------------------------------------
    tmpEvents.push(s.events);		// append any existing s.events string to the end of the temp events array (otherwise would overwrite).
    tmpEvents.push('purchase'); 	// add the 'purchase' event.
    s.events = tmpEvents.join();	// reduce the events array down to a comma separated string.

    // HANDLE EXTRA EVARS ---------------------------------------------------------
    // stores the cart total for use in bucket reports, which show breakdown of cost ranges (eg: $0-$100, $100.01-$200 etc).
    s[evars.currentCart] = cartTotal; 	// cart total for this transaction.
    s[evars.cartToDate] = '+'+cartTotal;	// this value should be a number prceded by a + sign.

    // Store the purchaseID into and Order ID report for performing breakdown reports   
    if(typeof(s.purchaseID) === "undefined")
    {	    
	    s_date=new Date;
	    s.purchaseID=s.transactionID=orderID=s_date.getTime();
    }
    s[evars.productID] = s[props.productID] = s.purchaseID; 
    
    // debugging info (if debugging == true, see config section at top of function)
    if(debugging) {
	    console.group("shoppingCart() debugging");
    	console.group("s.products");
    	console.log(s.products);
    	console.groupEnd();
    	console.group("s.events");
    	console.log(s.events);
    	console.groupEnd();
    	console.groupEnd();
    }
}

