SiteCatalyst plugin for simplifying e-commerce product strings.

Usage:

1) Add shoppingCart() method to the Custom Code Javascript section of your s code.
2) Within plugins section, add s.shoppingCart = shoppingCart to add the shoppingCart method to the S object.
3) Define a cart on your final purchase page.
eg:

s.contextData["cart"] = new Array;
s.contextData["cart"] = [
			{type:"product",id:"jeans555",units:1,revenue:100}, // 1x Shirt
			{type:"product",id:"shirt555",units:2,revenue:200}, // 2x Knits
			{type:"voucher",id:"7777777777",description:"$100 gift card",amount:100.00}, // $100 Giftcard
			{type:"shipping",description:"Domestic shipping",amount:12.95}, // Shipping
			{type:"discount",description:"Save $100 for every $300 spent",amount:100.00} // Cart discount
];

4) Call s.shoppingCart();

